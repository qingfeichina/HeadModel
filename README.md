HeadModel_V2.0介绍
============================================================

**版本修订**

| 系统版本号 | 文档版本号 | 修订日期   | 修订人 | 修订内容 |
| ---------- | ---------- | ---------- | ------ | -------- |
| v2.0       | v1.0       | 2023-04-05 | 李京   | 初版创建 |

# 1. 硬件系统

HeadModel_2.0在原有的V1.0框架下，升级成以地平线x3pi/英伟达Jetson Nano/英伟达Jetson Xavier等系列平台为基础的新一代机器人脑控系统，具体实现的功能如下：

* 支持双目取景，根据硬件平台要求可能是`单目USB摄像头单目MIPI摄像头`或者`双目MIPI摄像头`配置
* 单路串口115200bps支持1串N头部舵机阵列通讯
* 支持10/100/1000Mbps以太网通讯
* 支持WIFI通讯
* 支持蓝牙通讯
* 支持40PIN扩展，根据扩展可以支持I2S的MIC拾音版的4路麦克风输入，2路音频播放输出
* 支持40PIN扩展，根据扩展可以支持额外的串口与机械臂通讯，最大支持921600bps
* 支持外扩添加4G/5G/Lora/IOT等模块

## 1.1. 地平线x3pi介绍

旭日X3派开发板提供了以太网、USB、摄像头、LCD、HDMI、40PIN等外围接口，方便用户进行图像多媒体、AI算法的开发和测试。开发板接口布局如下：

![输入图片说明](documents/figures/image-dpx-1.png)

| 序号 | 功能                   | 序号 | 功能                | 序号 | 功能                 |
| ---- | ---------------------- | ---- | ------------------- | ---- | -------------------- |
| 1    | USB Type C 供电接口    | 6    | USB 3.0 Type A接口  | 11   | Wi-Fi天线接口        |
| 2    | MIPI CSI 摄像头接口    | 7    | 千兆以太网口        | 12   | TF卡接口（底面）     |
| 3    | 调试串口               | 8    | 40PIN接口           | 13   | MIPI 接口的LCD屏接口 |
| 4    | Micro USB 2.0 接口     | 9    | HDMI接口            | 14   | 触摸屏接口           |
| 5    | USB 2.0 Type A接口两路 | 10   | 电源和状态LED指示灯 |      |                      |

## 1.2. 地平线x3pi扩展40PIN介绍

旭日X3派开发板提供40PIN标准接口，方便用户进行外围扩展，其中数字IO采用3.3V电平设计。40PIN接口定义如下：

![输入图片说明](documents/figures/image-dpx-2.png)    

旭日X3派预置了GPIO Python库Hobot.GPIO，用户可以通过导入模块查看版本信息。


## 1.2. 英伟达Jetson Nano介绍

![输入图片说明](documents/figures/image-ywd-nano1.jpg)

![输入图片说明](documents/figures/image-ywd-nano2.jpg)

![输入图片说明](documents/figures/image-ywd-nano3.jpg)

## 1.3. 横向比较

| 配置         | 地平线x3-SD                                        | Jetson NANO                                        | Xavier NX2                                                   |
| ------------ | -------------------------------------------------- | -------------------------------------------------- | ------------------------------------------------------------ |
| **处理器**   | 地平线X3M芯片                                      | NVIDIA Maxwell架构                                   | NVIDIA Volta 架构                                                            |
| **CPU**      | 4核Cortex A53@1.2G                                 | 4核Cortex A57@1.43G                               | 6核 NVIDIA ARMV8                                             |
| **BPU/GPU**  | 2核BPU@1.0G</br>等效5TOPS                          | 128核Maxwell</br>等效0.5TOPS                        | 384CUDA+48Tensor Cores</br>等效14TOPS                        |
| **存储**     | 2GB LPDDR4 3200Mhz，16GB eMMC                      | 4GB LPDDR4 3200Mhz，16GB eMMC                      | 8GB LPDDR4 3200Mhz，16GB eMMC                                |
| **摄像头**   | MIPI CSI x2，支持双路摄像头接入                    | MIPI CSI x2，支持双路摄像头接入                    | MIPI CSI x2，支持双路摄像头接入                              |
| **显示**     | HDMI x1、MIPI DSI x1，最大支持1080p60              | HDMI x1、MIPI DSI x1，最大支持1080p60              | HDMI x1、MIPI DSI x1，最大支持1080p60                        |
| **USB**      | Host：USB 3.0 x1、USB 2.0 x2，Device: Micro USB x1 | Host：USB 3.0 x1、USB 2.0 x2，Device: Micro USB x1 | Host：USB 3.0 x2、USB 2.0 x2，Device: Micro USB x1           |
| **有线网络** | 千兆以太网 x1，RJ45接口                            | 千兆以太网 x1，RJ45接口                            | 千兆以太网 x1，RJ45接口                                      |
| **无线网络** | 2.4G Wi-Fi x1支持802.11 b/g/n</br>Bluetooth 4.1    | 2.4G Wi-Fi x1支持802.11 b/g/n</br>Bluetooth 4.1    | 2.4G Wi-Fi x1支持802.11 b/g/n</br>Bluetooth 4.1              |
| **其他接口** | TF卡接口 x1</br>40 Pin接口 x1                      | M.2 key E(wiFi/BT included)</br>40 Pin接口 x1      | M.2 key E(wiFi/BT included)</br>M.2 Key M(NVME)</br>40 Pin接口 x1 |
| **供电**     | 12V DC or USB Type C                               | 15V DC                                             | 15V DC                                                       |
| **尺寸**     | 91mmX75mmX30mm                                     | 100mmX80mmX29mm                                    | 103mmX90.5mmX34mm                                            |
| **价格**     | 1000                                               | 1229                                               | 4299                                                         |

## 1.4. 扩展板样例介绍

![输入图片说明](documents/figures/kzb-1.jpg)

本次改版新制40PIN扩展板，该板通过40PIN接入主控板后，支持降压模块，头模电机群接口模块，麦克风阵列，音效模块和机械臂主控板串口接口